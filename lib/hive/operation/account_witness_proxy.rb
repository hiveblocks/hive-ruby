class Hive::Operation::AccountWitnessProxy < Hive::Operation
  def_attr account: :string
  def_attr proxy: :string
end
